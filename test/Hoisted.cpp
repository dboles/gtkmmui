#include "gtkmmui/Hoisted.hpp"
#include "gtkmmui/application.hpp"
#include "gtkmmui/bind.hpp"
#include "gtkmmui/child.hpp"
#include "gtkmmui/property.hpp"
#include "gtkmmui/widget.hpp"
#include "gtkmmui/window.hpp"
#include <gtkmm/application.h>
#include <gtkmm/box.h>
#include <gtkmm/label.h>
#include <gtkmm/stack.h>
#include <gtkmm/stackswitcher.h>
#include <cstdlib>

static void on_activate(Gtk::Application& app)
{
    using namespace djb::gtkmmui; using namespace Gtk;

    auto stack = Hoisted<Stack>{};
    auto label = Hoisted<Label>{};

    auto win = window<Window>( app,
        child<Box>(
            child<StackSwitcher>(
                property(&StackSwitcher::property_stack, stack),
                bind(label, &Widget::property_visible, &Widget::property_visible)
            ),
            child<Stack>(
                is(stack)
            ),
            child<Label>(
                bind(stack, &Stack::property_visible_child_name, &Label::property_label)
            )
        )
    );

    app.remove_window(*win);
    win.reset();
    auto threw = false;
    try {
        static_cast<void>( widget<Label>(
            bind(stack, &Stack::property_visible_child_name, &Label::property_label)
        ) );
    } catch (std::runtime_error const&) { threw = true; }
    if (not threw) std::exit(EXIT_FAILURE);
}

auto main() -> int
{
    return djb::gtkmmui::run_application("org.djb.gtkmmui.test.Hoisted", &on_activate);
}
