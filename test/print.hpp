#ifndef DJB_GTKMMUI_TEST_PRINT_HPP
#define DJB_GTKMMUI_TEST_PRINT_HPP

#include "gtkmmui/format.hpp"
#include <fmt/ranges.h>
#include <tuple>

namespace djb::gtkmmui {

template <typename... Types>
void
print(Types const&... types)
{
	fmt::print( "{}\n", fmt::join(std::tie(types...), "\t") );
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_TEST_PRINT_HPP
