#include "gtkmmui.hpp"

#include <fmt/core.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/applicationwindow.h>
#include <gtkmm/box.h>
#include <gtkmm/button.h>
#include <gtkmm/entry.h>
#include <gtkmm/entrybuffer.h>
#include <gtkmm/gesturelongpress.h>
#include <gtkmm/gesturepan.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/label.h>
#include <gtkmm/scale.h>
#include <gtkmm/stack.h>
#include <gtkmm/stackswitcher.h>

namespace {

[[nodiscard]] auto
make_window(Gtk::Application& application)
{
    using namespace djb::gtkmmui;
    using namespace Glib;
    using namespace Gtk;

    auto the_stack = Hoisted<Stack>{};

    return window<ApplicationWindow>( application,
        bind(the_stack, &Stack::property_visible_child_name,
             &Window::property_title, false,
             [](ustring const& source){ return fmt::format("child: '{}'", source); } ),

        titlebar<HeaderBar>(
            bind(the_stack, &Stack::property_visible_child_name,
                 &HeaderBar::property_subtitle, false),

            property(&HeaderBar::property_show_close_button, true),

            child<Box, &HeaderBar::pack_end>(
                property(&Orientable::property_orientation, ORIENTATION_HORIZONTAL),

                child<StackSwitcher>(
                    property(&StackSwitcher::property_stack, the_stack)
                ),

                child<Stack>(
                    is(the_stack),

                    property(&Stack::property_transition_type, STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT),
                    property(&Stack::property_transition_duration, 250),

                    children( 2, [](int const i)
                    {
                        auto const n = fmt::to_string(i + 1);

                        return child<Label>(
                            child_property( &Stack::child_property_name, fmt::format("label_{}", n),
                                            [](Stack const&, Label const&){ fmt::print("both\n"); } ),

                            child_property( &Stack::child_property_title, fmt::format("Stack child {}", n) ),
                            child_notify  ( &Stack::child_property_title, &Container::set_focus_child       ),

                            property( &Label::property_label, fmt::format("Label {}", n) )
                        );
                    } )
                )
            )
        ),

        child<Box>(
            create(ORIENTATION_VERTICAL),
            notify( &Orientable::property_orientation,
                    [](Box const&){ fmt::print("Box\n"); } ),

            property(&Widget::property_halign, ALIGN_CENTER),

            child<Grid>(
                children( 4, [](int const i)
                {
                    auto const left_attach = 1 + i % 2;
                    auto const  top_attach = 1 + i / 2;

                    return grid_child<Label>(
                        left_attach, top_attach,
                        property( &Label::property_label, fmt::format("{0},{1}", left_attach, top_attach) ),
                        property(&Widget::property_hexpand, true)
                    );
                } )
            ),

            child<Entry>(),

            child<Entry>(
                property( &Entry::property_buffer, object<EntryBuffer>() )
            ),

            combobox(
                list_store<int, ustring, double, Icon>(
                    row( 1, "hi"    ,  99.9   , "audio-volume-high"  ),
                    row(11, "choose",  77.7   , "audio-volume-low"   ),
                    row( 2, "one"   ,  42.0   , "audio-volume-medium"),
                    row(22, "CHOOSE", 666.666 , "audio-volume-muted" ),
                    row(42, "ONE"   , 867.5309, "go-home"            )
                ),

                notify<CallNow::yes>(&ComboBox::property_active, &Widget::error_bell)
            ),

            child<Scale>(
                property( &Range::property_adjustment, object<Adjustment>(
                    create(0, 0, 0, 0, 0, 0),
                    property(&Adjustment::property_lower         ,   0),
                    property(&Adjustment::property_upper         , 100),
                    property(&Adjustment::property_step_increment,   1),
                    property(&Adjustment::property_page_increment,  10),
                    property(&Adjustment::property_page_size     ,  25),
                    property(&Adjustment::property_value         ,  50)
                ).get() ),
                property(&Scale::property_value_pos, POS_RIGHT)
            ),

            child<Button>(
                property("action-name", "app.quit"),
                property(&Widget::property_halign, ALIGN_CENTER),
                property(&Button::property_use_underline, true),
                property(&Button::property_label, "Click to _quit"),

                signal( &Widget::signal_enter_notify_event, [](Button const&,
                                                               GdkEventCrossing const*)
                {
                        fmt::print("Widget::enter_notify\n");
                        return false;
                } ),

                controller<GestureLongPress>(
                    property(&GestureSingle::property_button, 1),
                    property(&GestureLongPress::property_delay_factor, 2.0),

                    signal( &Gesture::signal_begin, [](GestureLongPress const&,
                                                       GdkEventSequence const*)
                    {
                        fmt::print("Gesture::begin\n");
                    } ),

                    signal( &GestureLongPress::signal_pressed,
                            [](GestureLongPress& gesture, double, double)
                    {
                        auto& button = dynamic_cast<Button&>( *gesture.get_widget() );
                        auto const styleContext = button.get_style_context();
                        styleContext->remove_class(GTK_STYLE_CLASS_DESTRUCTIVE_ACTION);
                        styleContext->   add_class(GTK_STYLE_CLASS_SUGGESTED_ACTION  );
                    } )
                ),

                controller<GesturePan>(
                    create(ORIENTATION_HORIZONTAL),
                    property(&GestureSingle::property_button, 3), // right-click

                    signal( &GestureDrag::signal_drag_begin, [](GesturePan const&,
                                                                double, double)
                    {
                        fmt::print("GestureDrag::signal_drag_begin\n");
                    } ),
                    signal( &GestureDrag::signal_drag_update, [](GesturePan const&,
                                                                 double, double)
                    {
                        fmt::print("GestureDrag::signal_drag_update\n");
                    } ),
                    signal( &GestureDrag::signal_drag_end, [](GesturePan const&,
                                                              double, double)
                    {
                        fmt::print("GestureDrag::signal_drag_end\n");
                    } ),

                    signal( &GesturePan::signal_pan,
                            [](GesturePan const&,
                               PanDirection, double)
                    {
                        fmt::print("GesturePan::signal_pan\n");
                    } )
                ),

                style_class(GTK_STYLE_CLASS_DESTRUCTIVE_ACTION,
                            GTK_STYLE_CLASS_FLAT)
            )
        )
    );
}

void
on_activate(Gtk::Application& application)
{
    application.add_action( "quit", [&]{ application.quit(); } );

    auto const window = make_window(application);
    window->show_all();
}

} // namespace

auto
main() -> int
{
    return djb::gtkmmui::run_application("org.djb.gtkmmui.test.test", &on_activate);
}
