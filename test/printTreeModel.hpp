#ifndef DJB_GTKMMUI_TEST_PRINTTREEMODEL_HPP
#define DJB_GTKMMUI_TEST_PRINTTREEMODEL_HPP

#include "print.hpp"
#include "gtkmmui/TreeModel.hpp"

#include <fmt/format.h>
#include <gtkmm/treepath.h>

#include <ranges>
#include <string>

namespace djb::gtkmmui {

template <typename Store, typename... Types>
void
print_flat(TreeModelData<Store, Types...> const& data)
{
	foreach( data, [&](Types const&... types)
	{
		print(types...);
		return false;
	} );
}

namespace detail {

template <typename... Types>
void
print_full(TreeModelSchema<Types...> const& schema,
           Gtk::TreePath const& path, Gtk::TreeIter const& it)
{
	auto const path_string = path.to_string();
	auto const depth = std::ranges::count(path_string, ':');
	auto const indentation = std::string(depth, '\t');
	fmt::print("{}{}:\t", indentation, path_string);
	std::apply( print<Types...>, get_values(schema, *it) );
}

} // namespace detail

template <typename Store, typename... Types>
void
print_full(TreeModelData<Store, Types...> const& data)
{
	data.store->foreach( [&](Gtk::TreePath const& path,
	                         Gtk::TreeIter const& it)
	{
		detail::print_full(data.schema, path, it);
		return false;
	} );
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_TEST_PRINTTREEMODEL_HPP
