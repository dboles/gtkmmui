#include "printTreeModel.hpp"

#include "gtkmmui/list_store.hpp"

#include "glibmm/ustring.h"
#include <gtkmm/application.h>

namespace {

void
test1()
{
	using namespace djb::gtkmmui;

	auto const listStore = list_store<int, Glib::ustring, Glib::ustring>(
		row( 1, "DB", "Daniel Boles"  ),
		row( 2, "HM", "Hidden Machine"),
		row(42, "IC", "Islay le Catte")
	);

	print_flat(listStore);

	foreach( listStore, [](auto const id, auto const code, auto name)
	{
		name = static_cast<Glib::ustring>(name) += " [updated]";
		print(id, code, name);
		return false;
	} );
}

void
test2()
{
	using namespace djb::gtkmmui;

	auto const listStore = list_store<char const*, double>(
		row("the answer",  42  ),
		row("Star Trek#",  47  ),
		row("Big Satan" , 666.6)
	);

	print_flat(listStore);
}

void
on_activate()
{
	test1();
	test2();
}

} // namespace

auto
main() -> int
{
	auto const application = Gtk::Application::create("org.djb.gtkmmui.test.list_store");
	application->signal_activate().connect(&on_activate);
	return application->run();
}
