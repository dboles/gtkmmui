#include "printTreeModel.hpp"

#include "gtkmmui/tree_store.hpp"

#include <glibmm/ustring.h>
#include <gtkmm/application.h>

namespace {

void
test1()
{
    using namespace djb::gtkmmui;

    auto const treeStore = tree_store<Glib::ustring, int>(
        node(
            data("Dad", 56),
            node(
                data("phone", 1)
            )
        ),
        node(
            data("Mum", 60),
            node(
                data("Sunny", 7)
            ),
            node(
                data("Daniel", 30),
                node(
                    data("Islay", 2)
                )
            )
        )
    );

    print_flat(treeStore);
    fmt::print("\n");
    print_full(treeStore);
}

void
test2()
{
    using namespace djb::gtkmmui;

    enum class Type {ch, op};

    auto const treeStore = tree_store<Type, int>(
        node(
            data(Type::ch, 1),
            node(
                data(Type::op, 1),
                node(
                    data(Type::op, 2)
                ),
                node(
                    data(Type::op, 3)
                )
            )
        ),
        node(
            data(Type::ch, 2),
            node(
                data(Type::op, 4),
                node(
                    data(Type::op, 5)
                ),
                node(
                    data(Type::op, 6)
                )
            )
        )
    );

    static_cast<void>(treeStore);
}

void
on_activate()
{
    test1();
    test2();
}

} // namespace

auto
main() -> int
{
    auto const application = Gtk::Application::create("org.djb.test");
    application->signal_activate().connect(&on_activate);
    return application->run();
}
