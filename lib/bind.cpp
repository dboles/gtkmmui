#include "gtkmmui/bind.hpp"
#include "gtkmmui/manage.hpp"

namespace djb::gtkmmui::detail {

void
manage_binding(Glib::RefPtr<Glib::Binding const> binding,
               Glib::PropertyProxy_Base const& source,
               Glib::PropertyProxy_Base const& target)
{
	manage(           binding , *source.get_object() );
	manage( std::move(binding), *target.get_object() );
}

} // namespace djb::gtkmmui::detail
