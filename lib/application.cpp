#include "gtkmmui/application.hpp"
#include <gtkmm/application.h>

#include <utility>

namespace djb::gtkmmui {

auto
run_application(Glib::ustring const& id, Gio::ApplicationFlags const flags,
                ActivateAppFunc on_activate) -> int
{
	auto const app = Gtk::Application::create(id, flags);
	app->signal_activate().connect( std::bind_front( std::move(on_activate),
	                                                 std::ref( *app.get() ) ) );
	return app->run();
}

auto
run_application(Glib::ustring const& id, ActivateAppFunc on_activate) -> int
{
	return run_application( id, Gio::APPLICATION_FLAGS_NONE, std::move(on_activate) );
}

} // namespace djb::gtkmmui
