#include "gtkmmui/ensure.hpp"
#include <fmt/core.h>
#include <stdexcept>

namespace djb::gtkmmui::detail {

void
ensure(bool const precondition, std::string_view const precondition_string,
       std::source_location const source_location)
{
	if (precondition) return;

	throw std::runtime_error{ fmt::format("{0}:{1}: {2}(): Precondition `{3}' failed.",
	                                      source_location.file_name(), source_location.line(),
	                                      source_location.function_name(), precondition_string) };
}

} // namespace djb::gtkmmui::detail
