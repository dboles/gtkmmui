#include "gtkmmui/manage.hpp"
#include "gtkmmui/ensure.hpp"

#include <glibmm/objectbase.h>
#include <glibmm/refptr.h>

#include <cassert>
#include <map>
#include <vector>

namespace djb::gtkmmui::detail {

namespace {

using Refs = std::vector< Glib::RefPtr<Glib::ObjectBase const> >;
auto s_map = std::map<Glib::ObjectBase const*, Refs>{};

[[nodiscard]] auto
erase(void* const data)
{
	auto const manager = const_cast<Glib::ObjectBase const*>(
			    static_cast<Glib::ObjectBase      *>(data) );
	[[maybe_unused]] auto const count = s_map.erase(manager);
	assert(count == 1);
	return data;
}

void
add_callback(Glib::ObjectBase const& manager)
{
	auto const data = static_cast<void            *>(
			   const_cast<Glib::ObjectBase*>(&manager) );
	manager.add_destroy_notify_callback(data, &erase);
}

} // namespace

void
manage(Glib::RefPtr<Glib::ObjectBase const> managed,
       Glib::ObjectBase const& manager)
{
	DJB_ENSURE(managed);
	auto const [it, inserted] = s_map.try_emplace(&manager);
	it->second.push_back( std::move(managed) );

	if (inserted) {
		add_callback(manager);
	}
}

} // namespace djb::gtkmmui::detail
