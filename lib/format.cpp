#include "gtkmmui/format.hpp"

#include <fmt/core.h>
#include <glib-object.h>
#include <glibmm/object.h>
#include <gtkmm/widget.h>

namespace djb::gtkmmui::detail {

auto
format(Glib::ObjectBase const& object) -> Glib::ustring
{
	auto const type_name = G_OBJECT_TYPE_NAME( object.gobj() );
	auto result = fmt::format( "{0} @ {1}", type_name, static_cast<void const*>(&object) );

	if ( auto const widget = dynamic_cast<Gtk::Widget const*>(&object) ) {
		if ( auto const name = widget->get_name(); not name.empty() ) {
			result += fmt::format( " '{}'", name.c_str() );
		}
	}

	return result;
}

} // namespace djb::gtkmmui::detail
