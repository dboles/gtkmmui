#include "gtkmmui/manage_window.hpp"
#include "gtkmmui/ensure.hpp"
#include <gtkmm/application.h>
#include <algorithm>
#include <utility>

namespace djb::gtkmmui {

// TODO: C++23: Use std::ranges::contains()
template <typename Range, typename Value>
[[nodiscard]] static auto
contains(Range const& range, Value const& value)
{
	return std::ranges::find(range, value) != std::ranges::end(range);
}

[[nodiscard]] static auto
contains(Gtk::Application const& application, Gtk::Window const& window)
{
	return contains(application.get_windows(), &window);
}

static void
manage(Gtk::Window& window, Gtk::Application& application)
{
	DJB_ENSURE( not contains(application, window) );

	application.add_window(window);

	application.signal_shutdown().connect( [&]
	{
		if ( contains(application, window) ) {
			application.remove_window(window);
		}
	} );
}

void
manage(std::shared_ptr<Gtk::Window> window,
       Gtk::Application& application)
{
	DJB_ENSURE(window);

	manage(*window, application);

	application.signal_window_removed().connect( [ window = std::move(window) ]
	                                             (auto const* const removed) mutable
	{
		if (window and window.get() == removed) {
			window.reset();
		}
	} );
}

} // namespace djb::gtkmmui
