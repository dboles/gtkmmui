#ifndef DJB_GTKMMUI_PENDING_HPP
#define DJB_GTKMMUI_PENDING_HPP

#include "Overload.hpp"
#include "ensure.hpp"
#include "format.hpp"
#include "warn.hpp"
#include "with.hpp"

#include <glibmm/refptr.h>

#include <typeinfo>
#include <variant>
#include <vector>

namespace djb::gtkmmui {

template <std::derived_from<Glib::ObjectBase> Object>
class Hoisted final {
	using Callbacks = std::vector< std::function< void (Object&) > >;
	using ObjectCalled = std::pair<std::reference_wrapper<Object>, bool>;
	std::variant<Callbacks, ObjectCalled, std::monostate> variant;

	[[nodiscard]] static auto destroyed(void* const data)
	{
		static_cast<Hoisted*>(data)->variant = std::monostate{};
		return data;
	}

public:
	[[nodiscard]] auto _call(std::invocable<Object&> auto func)
	{
		return [ &, func = std::move(func) ]
		       (std::derived_from<Glib::ObjectBase> auto&... others)
		{
			using detail::with;
			std::visit(Overload{
				[&](Callbacks& callbacks)
				{
					callbacks.push_back( [&, func](Object& object)
					{ with( others..., std::invoke(func, object) ); } );
				},
				[&](ObjectCalled& object_called)
				{
					auto& [object, called] = object_called;
					with( others..., std::invoke( func, object.get() ) );
					called = true;
				},
				[](std::monostate)
				{
					warn("{0} is destroyed!\n", typeid(Object).name() );
					DJB_ENSURE(false);
				}
			}, variant);
		};
	}

	void _is(Object& object)
	{
		auto const& callbacks = std::get<Callbacks>(variant);
		for (auto const& callback: callbacks) std::invoke(callback, object);
		variant.template emplace<ObjectCalled>( object, not callbacks.empty() );
		object.add_destroy_notify_callback(this, destroyed);
	}

	void _is(Glib::RefPtr<Object> const& object){ is(*object); }

	~Hoisted()
	{
		std::visit(Overload{
			[](Callbacks const& callbacks)
			{
				warn( "No {0} was provided, but {1} Callback(s) need it!\n",
				      typeid(Object).name(), callbacks.size() );
			},
			[&](ObjectCalled const& object_called)
			{
				auto const& [object, called] = object_called;
				object.get().remove_destroy_notify_callback(this);
				if (not called) {
					warn( "Nothing was called for `{}`!\n",
					      detail::format(object_called.first) );
				}
			},
			[](std::monostate){} // itʼs deid, but so are we, so OK – do nothing
		}, variant);
	}
};

template <std::derived_from<Glib::ObjectBase> Object>
[[nodiscard]] auto
is(Hoisted<Object>& hoisted_object)
{
	return [&](auto& object){ hoisted_object._is(object); };
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_PENDING_HPP
