#ifndef DJB_GTKMMUI_NOTIFY_HPP
#define DJB_GTKMMUI_NOTIFY_HPP

#include "CallNow.hpp"
#include "PropertyGetter.hpp"

#include <glibmm/objectbase.h>
#include <sigc++/functors/mem_fun.h>

#include <concepts>
#include <functional>
#include <utility>

namespace djb::gtkmmui {

template <CallNow call_now = CallNow::no,
          typename... Callbacks>
[[nodiscard]] auto
notify(Glib::ustring name, Callbacks... callbacks)
{
	return [ name = std::move(name), ...callbacks = std::move(callbacks) ]
	       (std::derived_from<Glib::ObjectBase> auto& object)
	{
		if constexpr (call_now == CallNow::yes)
			(std::invoke(callbacks, object), ...);

		(object.connect_property_changed( name,
			std::bind_front( callbacks, std::ref(object) ) ), ...);
	};
}

template <CallNow call_now = CallNow::no,
          std::derived_from<Glib::ObjectBase> Object, typename Value,
          typename... Callbacks>
[[nodiscard]] auto
notify(PropertyGetterConst<Object, Value> const getter, Callbacks... callbacks)
{
	return [ getter, ...callbacks = std::move(callbacks) ]
	       (std::derived_from<Object> auto& object)
	{
		if constexpr (call_now == CallNow::yes)
			(std::invoke(callbacks, object), ...);

		auto proxy = (object.*getter)();
		auto signal = proxy.signal_changed();
		(signal.connect(
			std::bind_front( callbacks, std::ref(object) ) ), ...);
	};
}

template <CallNow call_now = CallNow::no,
          std::derived_from<Glib::ObjectBase> Object, typename Value,
          typename MemObj, typename MemRes, typename... MemArgs>
[[nodiscard]] auto
notify(PropertyGetterConst<Object, Value> const getter,
       MemObj& mem_obj, MemRes (MemObj::* const mem_ptr)(MemArgs...) )
{
	return notify<call_now>( getter, sigc::mem_fun(mem_obj, mem_ptr) );
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_NOTIFY_HPP
