#ifndef DJB_GTKMMUI_ENSURE_HPP
#define DJB_GTKMMUI_ENSURE_HPP

#include <source_location>
#include <string_view>

namespace djb::gtkmmui::detail {

void ensure( bool, std::string_view,
             std::source_location = std::source_location::current() );

} // namespace djb::gtkmmui::detail

#define DJB_ENSURE(PRECONDITION) \
	detail::ensure(static_cast<bool>(PRECONDITION), #PRECONDITION);

#endif // DJB_GTKMMUI_ENSURE_HPP
