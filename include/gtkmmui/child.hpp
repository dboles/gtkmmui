#ifndef DJB_GTKMMUI_CHILD_HPP
#define DJB_GTKMMUI_CHILD_HPP

#include "IsNonToplevelWidget.hpp"
#include "create.hpp"
#include "with.hpp"

#include <gtkmm/container.h>

#include <concepts>
#include <functional>
#include <utility>

namespace djb::gtkmmui {

template <IsNonToplevelWidget Child,
          auto adder = &Gtk::Container::add,
          typename... Args,
          typename... Functions>
requires std::constructible_from<Child, Args...>
[[nodiscard]] auto
child(detail::CreateResult<Args...> create_args,
      Functions... functions)
{
	return [ create_args = std::move(create_args),
	         ...functions = std::move(functions) ]
	       <std::derived_from<Gtk::Container> Parent>
	       requires ( std::invocable<decltype(adder), Parent&, Child&> and
		          (detail::invocable_1or2<Functions, Child, Parent> and ...) )
	       (Parent& parent)
	{
		auto& child = *std::apply(Gtk::make_managed<Child, Args const&...>,
		                          create_args.tuple);
		std::invoke(adder, parent, child);
		detail::with(child, parent, functions...);
	};
}

template <IsNonToplevelWidget Child,
          auto adder = &Gtk::Container::add>
[[nodiscard]] auto
child(detail::IsNotCreateResult auto... functions)
{
	return child<Child, adder>(detail::CreateResult<>{}, std::move(functions)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_CHILD_HPP
