#ifndef DJB_GTKMMUI_FORMAT_HPP
#define DJB_GTKMMUI_FORMAT_HPP

#include <fmt/format.h>
#include <glibmm/ustring.h>

namespace Glib { class ObjectBase; }

template <>
struct fmt::formatter<Glib::ustring>: formatter<string_view>
{
	[[nodiscard]] auto format(Glib::ustring const& ustr, auto& formatContext) const
	{ return formatter<string_view>::format(ustr.c_str(), formatContext); }
};

namespace djb::gtkmmui::detail {

[[nodiscard]] auto format(Glib::ObjectBase const& object) -> Glib::ustring;

} // namespace djb::gtkmmui::detail

#endif // DJB_GTKMMUI_FORMAT_HPP
