#ifndef DJB_GTKMMUI_TREE_STORE_HPP
#define DJB_GTKMMUI_TREE_STORE_HPP

#include "TreeModel.hpp"
#include "forward.hpp"

#include <gtkmm/treestore.h>

#include <concepts>
#include <optional>
#include <utility>

namespace djb::gtkmmui {

namespace detail {

template <typename Function, typename... Types>
concept TreeStoreInvocable = std::invocable<Function,
                                            TreeModelData<Gtk::TreeStore, Types...> const&,
                                            Gtk::TreeRow const&>;

} // namespace detail

template <typename... Types>
[[nodiscard]] auto
tree_store(detail::TreeModelInvocable<Gtk::TreeStore, Types...> auto&&... functions)
{
	return detail::store<Gtk::TreeStore, Types...>(FORWARD(functions)...);
}

template <typename... Functions>
[[nodiscard]] auto
node(Functions... functions)
{
	return [ ...functions = std::move(functions) ]<typename... Types>
	       ( TreeModelData<Gtk::TreeStore, Types...> const& data,
	         std::optional<Gtk::TreeRow> const row = {} )
	       requires (detail::TreeStoreInvocable<Functions, Types...> and ...)
	{
		auto const child_row = *(row ? data.store->append( row->children() )
		                             : data.store->append() );
		(std::invoke(functions, data, child_row), ...);
	};
}

template <typename... Types>
[[nodiscard]] auto
data(Types... values)
{
	return detail::tree_model_setter<Gtk::TreeStore>(std::move(values)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_TREE_STORE_HPP
