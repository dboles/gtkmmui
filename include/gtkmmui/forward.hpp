#ifndef DJB_GTKMMUI_FORWARD_HPP
#define DJB_GTKMMUI_FORWARD_HPP

#include <utility>

namespace djb::gtkmmui {

#define FORWARD(ARG) std::forward< decltype(ARG) >(ARG)

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_FORWARD_HPP
