#ifndef DJB_GTKMMUI_COMBOBOX_HPP
#define DJB_GTKMMUI_COMBOBOX_HPP

#include "child.hpp"
#include "list_store.hpp"
#include <glibmm/ustring.h>
#include <gtkmm/combobox.h>
#include <concepts>
#include <utility>

namespace djb::gtkmmui {

class Icon final {
	Glib::ustring name;

public:
	Icon() = default;
	Icon(auto string): name{ std::move(string) } {}
	auto& get_name() const { return name; }
};

namespace detail {

template <typename Type>
void
pack(Gtk::ComboBox& combo, Gtk::TreeModelColumn<Type> const& column)
{
	combo.pack_start(column);
}

template <std::derived_from<Gtk::CellRenderer> Cell, typename Value>
void
pack(Gtk::ComboBox& combo, Gtk::TreeModelColumn<Value> const& column,
     std::invocable<Cell&, Value const&> auto callback)
{
	auto const cell = Gtk::make_managed<Cell>();
	combo.pack_start(*cell);
	combo.set_cell_data_func( *cell, [ =, callback = std::move(callback) ]
	                                 (Gtk::TreeModel::const_iterator const it)
	                                 { std::invoke( callback, *cell, it->get_value(column) ); } );
}

void
pack(Gtk::ComboBox& combo, Gtk::TreeModelColumn<Icon> const& icon)
{
	using Cell = Gtk::CellRendererPixbuf;
	pack<Cell>( combo, icon, [](Cell& cell, Icon const& icon)
	                         { cell.property_icon_name() = icon.get_name(); } );
}

} // namespace detail

template <std::derived_from<Gtk::TreeModel> Store, typename... Types,
          typename... Functions>
[[nodiscard]] auto
combobox(TreeModelData<Store, Types...> const& data, Functions... functions)
{
	auto const set_model = [&](Gtk::ComboBox& combo)
	{
		combo.set_model(data.store);
		data.schema.with_columns( [&](auto&... columns)
			{ (detail::pack(combo, columns), ...); } );
	};

	return child<Gtk::ComboBox>(set_model, std::move(functions)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_COMBOBOX_HPP
