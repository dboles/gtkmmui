#ifndef DJB_GTKMMUI_PROPERTY_HPP
#define DJB_GTKMMUI_PROPERTY_HPP

#include "C2Ustring.hpp"
#include "CallNow.hpp"
#include "PropertyGetter.hpp"

#include <glibmm/objectbase.h>

#include <concepts>
#include <functional>
#include <type_traits>
#include <utility>

namespace djb::gtkmmui {

template <CallNow call_now = CallNow::no,
          typename Value,
          typename... Callbacks>
[[nodiscard]] auto
property(Glib::ustring name, Value value,
         Callbacks... callbacks)
{
	return [ name = std::move(name),
	         value = C2Ustring<Value>{ std::move(value) },
	         ...callbacks = std::move(callbacks) ]
	       (Glib::ObjectBase& object)
	{
		object.set_property(name, value);

		if constexpr (call_now == CallNow::yes)
			(std::invoke(callbacks, object), ...);

		(object.connect_property_changed(name, callbacks), ...);
	};
}

template <CallNow call_now = CallNow::no,
          std::derived_from<Glib::ObjectBase> Object, typename Value,
          typename... Callbacks>
[[nodiscard]] auto
property(PropertyGetter<Object, Value> const getter,
         std::type_identity_t<Value> value,
         Callbacks... callbacks)
{
	return [ getter, value = std::move(value),
	         ...callbacks = std::move(callbacks) ]
	       (Object& object)
	{
		auto proxy = (object.*getter)();
		proxy.set_value(value);

		if constexpr (call_now == CallNow::yes)
			(std::invoke(callbacks, object), ...);

		auto signal = proxy.signal_changed();
		(signal.connect(
			std::bind_front( callbacks, std::ref(object) ) ), ...);
	};
}

template <CallNow call_now = CallNow::no,
          std::derived_from<Glib::ObjectBase> Value,
          typename... Callbacks>
[[nodiscard]] auto
property(Glib::ustring name, Hoisted<Value>& hoisted_value,
         Callbacks... callbacks)
{
	return hoisted_value._call(
	                       [ name = std::move(name),
	                         ...callbacks = std::move(callbacks) ]
	                       (Value& value)
	{
		return property<call_now>(name, &value,
		                          callbacks...);
	} );
}

template <CallNow call_now = CallNow::no,
          std::derived_from<Glib::ObjectBase> Object      ,
          std::derived_from<Glib::ObjectBase> Value       ,
          std::derived_from<Glib::ObjectBase> HoistedValue,
          typename... Callbacks>
[[nodiscard]] auto
property(PropertyGetter<Object, Value*> const getter,
         Hoisted<HoistedValue>& hoisted_value,
         Callbacks... callbacks)
{
	return hoisted_value._call(
	                       [ getter,
	                         ...callbacks = std::move(callbacks) ]
	                       (Value& value)
	{
		return property<call_now>(getter, &value,
		                          callbacks...);
	} );
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_PROPERTY_HPP
