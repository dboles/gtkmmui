#ifndef DJB_GTKMMUI_ISNONTOPLEVELWIDGET_HPP
#define DJB_GTKMMUI_ISNONTOPLEVELWIDGET_HPP

#include "IsToplevel.hpp"
#include <gtkmm/widget.h>
#include <concepts>

namespace djb::gtkmmui {

template <typename T>
concept IsNonToplevelWidget = std::derived_from<T, Gtk::Widget> and
                              not IsToplevel<T>;

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_ISNONTOPLEVELWIDGET_HPP
