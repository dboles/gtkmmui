#ifndef DJB_GTKMMUI_SIGNALGETTER_HPP
#define DJB_GTKMMUI_SIGNALGETTER_HPP

#include <glibmm/objectbase.h>
#include <glibmm/signalproxy.h>

#include <concepts>

namespace djb::gtkmmui {

template <std::derived_from<Glib::ObjectBase> Object,
          typename Result, typename... Args>
using SignalGetter = Glib::SignalProxy<Result, Args...> (Object::*)();

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_SIGNALGETTER_HPP
