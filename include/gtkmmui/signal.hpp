#ifndef DJB_GTKMMUI_SIGNAL_HPP
#define DJB_GTKMMUI_SIGNAL_HPP

#include "SignalGetter.hpp"

#include <glibmm/objectbase.h>
#include <sigc++/adaptors/bind.h>
#include <sigc++/functors/mem_fun.h>

#include <concepts>
#include <functional>
#include <utility>

namespace djb::gtkmmui {

template <std::derived_from<Glib::ObjectBase> Object,
          typename Result, typename... Args,
          typename... Callbacks>
requires (sizeof...(Callbacks) > 0)
[[nodiscard]] auto
signal(SignalGetter<Object, Result, Args...> const getter,
       Callbacks... callbacks)
{
	return [ getter, ...callbacks = std::move(callbacks) ]
	       (std::derived_from<Object> auto& object)
	{
		auto proxy = (object.*getter)();
		(proxy.connect(
			// std::bind_front() does not work, at least in sigc++-3
			sigc::bind<0>( callbacks, std::ref(object) )
		), ...);
	};
}

template <std::derived_from<Glib::ObjectBase> Object,
          typename Result, typename... Args,
          typename MemObj, typename MemRes, typename... MemArgs>
[[nodiscard]] auto
signal( SignalGetter<Object, Result, Args...> const getter,
        MemObj& mem_obj, MemRes (MemObj::* const mem_ptr)(MemArgs...) )
{
	return signal( getter, sigc::mem_fun(mem_obj, mem_ptr) );
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_SIGNAL_HPP
