#ifndef DJB_GTKMMUI_WIDGET_HPP
#define DJB_GTKMMUI_WIDGET_HPP

#include "IsNonToplevelWidget.hpp"
#include "create.hpp"
#include "forward.hpp"

#include <concepts>
#include <functional>
#include <utility>

namespace djb::gtkmmui {

template <IsNonToplevelWidget Widget,
          typename... Args>
requires std::constructible_from<Widget, Args...>
[[nodiscard]] auto&
widget(detail::CreateResult<Args...> create_args,
       std::invocable<Widget&> auto&&... functions)
{
	auto& widget = *std::apply( Gtk::make_managed<Widget, Args...>,
	                            std::move(create_args.tuple) );
	(std::invoke(FORWARD(functions), widget), ...);
	return widget;
}

template <IsNonToplevelWidget Widget>
[[nodiscard]] auto&
widget(detail::IsNotCreateResult auto&&... functions)
{
	return widget<Widget>(detail::CreateResult<>{}, FORWARD(functions)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_WIDGET_HPP
