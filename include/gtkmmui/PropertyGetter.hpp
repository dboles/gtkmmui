#ifndef DJB_GTKMMUI_PROPERTYGETTER_HPP
#define DJB_GTKMMUI_PROPERTYGETTER_HPP

#include <glibmm/objectbase.h>
#include <glibmm/propertyproxy.h>

#include <concepts>

namespace djb::gtkmmui {

template <std::derived_from<Glib::ObjectBase> Object, typename Value>
using PropertyGetterConst = Glib::PropertyProxy_ReadOnly<Value>
                            (Object::*)() const;

template <std::derived_from<Glib::ObjectBase> Object, typename Value>
using PropertyGetter = Glib::PropertyProxy<Value> (Object::*)();

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_PROPERTYGETTER_HPP
