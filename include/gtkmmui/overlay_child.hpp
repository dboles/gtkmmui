#ifndef DJB_GTKMMUI_OVERLAY_CHILD_HPP
#define DJB_GTKMMUI_OVERLAY_CHILD_HPP

#include "create.hpp"
#include "widget.hpp"

#include <gtkmm/overlay.h>
#include <gtkmm/widget.h>

#include <concepts>
#include <utility>

namespace djb::gtkmmui {

template <std::derived_from<Gtk::Widget> Child,
          typename... Args>
requires std::constructible_from<Child, Args...>
[[nodiscard]] auto
overlay_child(detail::CreateResult<Args...> create_args,
              std::invocable<Child&> auto... functions)
{
	return [ create_args = std::move(create_args),
	         ...functions = std::move(functions) ]
	       (Gtk::Overlay& overlay)
	{
		auto& child = widget<Child>(create_args,
		                            functions...);
		overlay.add_overlay(child);
	};
}

template <std::derived_from<Gtk::Widget> Child>
[[nodiscard]] auto
overlay_child(detail::IsNotCreateResult auto... functions)
{
	return overlay_child<Child>(detail::CreateResult<>{},
	                            std::move(functions)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_OVERLAY_CHILD_HPP
