#ifndef DJB_GTKMMUI_OBJECT_HPP
#define DJB_GTKMMUI_OBJECT_HPP

#include "create.hpp"
#include "forward.hpp"

#include <glibmm/objectbase.h>
#include <gtkmm/widget.h>

#include <concepts>
#include <functional>
#include <utility>

namespace djb::gtkmmui {

template <typename T>
concept IsObjectNotWidget =     std::derived_from<T, Glib::ObjectBase> and
                            not std::derived_from<T, Gtk::Widget     >;

template <IsObjectNotWidget Object, typename... Args>
[[nodiscard]] auto
object(detail::CreateResult<Args...> create_args,
       std::invocable<Object&> auto&&... functions)
{
	auto object = std::apply( [](auto... args){ return Object::create(std::move(args)...); },
	                          std::move(create_args.tuple) );
	(std::invoke( FORWARD(functions), *object.get() ), ...);
	return object;
}

template <IsObjectNotWidget Object>
[[nodiscard]] auto
object(detail::IsNotCreateResult auto&&... functions)
{
	return object<Object>(detail::CreateResult<>{}, FORWARD(functions)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_OBJECT_HPP
