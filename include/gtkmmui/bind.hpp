#ifndef DJB_GTKMMUI_BIND_HPP
#define DJB_GTKMMUI_BIND_HPP

#include "Hoisted.hpp"
#include "PropertyGetter.hpp"

#include <glibmm/binding.h>
#include <glibmm/objectbase.h>
#include <glibmm/propertyproxy.h>

#include <cstddef>
#include <optional>
#include <type_traits>
#include <utility>

namespace djb::gtkmmui {

template <typename Function, typename Source, typename Target>
concept IsTransform = std::same_as<Function, std::nullptr_t> or
	std::is_invocable_r_v<std::optional<Target>, Function, Source const&>;

namespace detail {

template <typename Source, typename Target,
          IsTransform<Source, Target> Transform>
[[nodiscard]] auto
mm(Transform const& transform)
-> Glib::Binding::SlotTypedTransform<Source, Target>
{
	if constexpr ( std::same_as<Transform, std::nullptr_t> ) {
		return {};
	} else {
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Waddress" // If callable always has address, #whocare
		if (not transform) return {};
#pragma GCC diagnostic pop

		return [transform](Source const& source, Target& target)
		{
			auto optional = std::optional{ transform(source) };
			return optional ? target = *std::move(optional), true : false;
		};
	}
}

[[nodiscard]] auto
get_flags(bool const bidirectional, bool const sync_create)
{
	return static_cast<Glib::BindingFlags>(
		bidirectional * Glib::BINDING_BIDIRECTIONAL |
		sync_create   * Glib::BINDING_SYNC_CREATE);
}

// https://gitlab.gnome.org/GNOME/glibmm/issues/62
void manage_binding(Glib::RefPtr<Glib::Binding const>,
                    Glib::PropertyProxy_Base const& source,
                    Glib::PropertyProxy_Base const& target);

template <typename Source, typename Target>
void
bind(Glib::PropertyProxy_ReadOnly<Source> const& source,
     Glib::PropertyProxy         <Target> const& target,
     bool const sync_create,
     IsTransform<Source, Target> auto const& transform)
{
	auto const flags = get_flags(false, sync_create);
	auto binding = Glib::Binding::bind_property(
		source, target, flags, mm<Source, Target>(transform) );

	manage_binding( std::move(binding), source, target );
}

template <typename Source, typename Target>
void
bind_bidirectional(Glib::PropertyProxy<Source> const& source,
                   Glib::PropertyProxy<Target> const& target,
                   bool const sync_create,
                   IsTransform<Source, Target> auto const& transform_to,
                   IsTransform<Target, Source> auto const& transform_from)
{
	auto const flags = get_flags(true, sync_create);
	auto binding = Glib::Binding::bind_property(
		source, target, flags, mm<Source, Target>(transform_to  ) ,
		                       mm<Target, Source>(transform_from) );

	manage_binding( std::move(binding), source, target );
}

} // namespace detail

template <std::derived_from<Glib::ObjectBase> SourceObject, typename SourceValue,
          std::derived_from<Glib::ObjectBase> TargetObject, typename TargetValue,
          IsTransform<SourceValue, TargetValue> Transform = std::nullptr_t      >
[[nodiscard]] auto
bind(SourceObject const& source_object,
     PropertyGetterConst<SourceObject, SourceValue> const source_property,
     PropertyGetter     <TargetObject, TargetValue> const target_property,
     bool const sync_create = false,
     Transform  transform   = {}   )
{
	return [ &source_object, source_property, target_property, sync_create,
	         transform = std::move(transform) ]
	       (TargetObject& target_object)
	{
		detail::bind( (source_object.*source_property)(),
		              (target_object.*target_property)(),
		              sync_create, transform );
	};
}

template <std::derived_from<Glib::ObjectBase> SourceObject, typename SourceValue,
          std::derived_from<Glib::ObjectBase> TargetObject, typename TargetValue,
          IsTransform<SourceValue, TargetValue> TransformTo   = std::nullptr_t  ,
          IsTransform<SourceValue, TargetValue> TransformFrom = std::nullptr_t  >
[[nodiscard]] auto
bind_bidirectional(SourceObject& source_object,
                   PropertyGetter<SourceObject, SourceValue> const source_property,
                   PropertyGetter<TargetObject, TargetValue> const target_property,
                   bool const    sync_create    = false,
                   TransformTo   transform_to   = {}   ,
                   TransformFrom transform_from = {}   )
{
	return [ &source_object, source_property, target_property, sync_create,
	         transform_to   = std::move(transform_to),
	         transform_from = std::move(transform_from) ]
	       (TargetObject& target_object)
	{
		detail::bind_bidirectional( (source_object.*source_property)(),
		                            (target_object.*target_property)(),
		                            sync_create, transform_to, transform_from );
	};
}

template <std::derived_from<Glib::ObjectBase> SourceObject, typename SourceValue,
          std::derived_from<Glib::ObjectBase> TargetObject, typename TargetValue,
          std::derived_from<SourceObject    > SourceHoisted                     ,
          IsTransform<SourceValue, TargetValue> Transform = std::nullptr_t      >
[[nodiscard]] auto
bind(Hoisted<SourceHoisted>& source_hoisted,
     PropertyGetterConst<SourceObject, SourceValue> const source_property,
     PropertyGetter     <TargetObject, TargetValue> const target_property,
     bool const sync_create = false,
     Transform  transform   = {}   )
{
	return source_hoisted._call(
	        [ source_property, target_property, sync_create,
	          transform = std::move(transform) ]
	        (SourceObject const& source_object)
	{
		return bind(source_object, source_property,
		            target_property, sync_create, transform);
	} );
}

template <std::derived_from<Glib::ObjectBase> SourceObject, typename SourceValue,
          std::derived_from<Glib::ObjectBase> TargetObject, typename TargetValue,
          std::derived_from<SourceObject    > SourceHoisted                     ,
          IsTransform<SourceValue, TargetValue> TransformTo   = std::nullptr_t  ,
          IsTransform<SourceValue, TargetValue> TransformFrom = std::nullptr_t  >
[[nodiscard]] auto
bind_bidirectional(Hoisted<SourceHoisted>& source_hoisted,
                   PropertyGetter<SourceObject, SourceValue> const source_property,
                   PropertyGetter<TargetObject, TargetValue> const target_property,
                   bool const    sync_create    = false,
                   TransformTo   transform_to   = {}   ,
                   TransformFrom transform_from = {}   )
{
	return source_hoisted._call(
	        [ source_property, target_property, sync_create,
	          transform_to   = std::move(transform_to  ),
	          transform_from = std::move(transform_from) ]
	        (SourceObject& source_object)
	{
		return bind_bidirectional(source_object, source_property,
		                          target_property,
		                          sync_create, transform_to, transform_from);
	} );
}

template <typename Value>
[[nodiscard]] auto
get_property(Glib::ObjectBase const& object, Glib::ustring const& name)
{
	return Glib::PropertyProxy_ReadOnly<Value>{ &object, name.c_str() };
}

template <typename Value>
[[nodiscard]] auto
get_property(Glib::ObjectBase& object, Glib::ustring const& name)
{
	return Glib::PropertyProxy<Value>{ &object, name.c_str() };
}

template <std::derived_from<Glib::ObjectBase> SourceObject, typename SourceValue,
          std::derived_from<Glib::ObjectBase> TargetObject, typename TargetValue,
          IsTransform<SourceValue, TargetValue> Transform = std::nullptr_t      >
[[nodiscard]] auto
bind(SourceObject const& source_object,
     Glib::ustring source_property,
     Glib::ustring target_property,
     bool const sync_create = false,
     Transform  transform   = {}   )
{
	return [ &source_object, source_property = std::move(source_property),
	         target_property = std::move(target_property),
	         sync_create, transform = std::move(transform) ]
	       (TargetObject& target_object)
	{
		detail::bind(get_property<SourceValue>(source_object, source_property),
		             get_property<TargetValue>(target_object, target_property),
		             sync_create, transform);
	};
}

template <std::derived_from<Glib::ObjectBase> SourceObject, typename SourceValue,
          std::derived_from<Glib::ObjectBase> TargetObject, typename TargetValue,
          IsTransform<SourceValue, TargetValue> TransformTo   = std::nullptr_t  ,
          IsTransform<SourceValue, TargetValue> TransformFrom = std::nullptr_t  >
[[nodiscard]] auto
bind_bidirectional(SourceObject& source_object,
                   Glib::ustring source_property,
                   Glib::ustring target_property,
                   bool const    sync_create    = false,
                   TransformTo   transform_to   = {}   ,
                   TransformFrom transform_from = {}   )
{
	return [ &source_object, sync_create,
	         source_property = std::move(source_property),
	         target_property = std::move(target_property),
	         transform_to    = std::move(transform_to   ),
	         transform_from  = std::move(transform_from ) ]
	       (TargetObject& target_object)
	{
		detail::bind_bidirectional(get_property<SourceValue>(source_object, source_property),
		                           get_property<TargetValue>(target_object, target_property),
		                           sync_create, transform_to, transform_from);
	};
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_BIND_HPP
