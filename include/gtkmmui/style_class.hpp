#ifndef DJB_GTKMMUI_STYLE_CLASS_HPP
#define DJB_GTKMMUI_STYLE_CLASS_HPP

#include <glibmm/ustring.h>
#include <gtkmm/stylecontext.h>
#include <gtkmm/widget.h>

#include <concepts>
#include <utility>

namespace djb::gtkmmui {

template <std::convertible_to<Glib::ustring>... Strings>
requires (sizeof...(Strings) > 0)
[[nodiscard]] auto
style_class(Strings... names)
{
	return [ ...names = Glib::ustring{ std::move(names) } ]
	       (Gtk::Widget& widget)
	{
		(widget.get_style_context()->add_class(names), ...);
	};
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_STYLE_CLASS_HPP
