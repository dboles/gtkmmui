#ifndef DJB_GTKMMUI_NAME_HPP
#define DJB_GTKMMUI_NAME_HPP

#include <glibmm/ustring.h>
#include <gtkmm/widget.h>

#include <utility>

namespace djb::gtkmmui {

[[nodiscard]] auto
name(Glib::ustring name)
{
	return [ name = std::move(name) ](Gtk::Widget& widget)
	{
		widget.set_name(name);
	};
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_NAME_HPP
