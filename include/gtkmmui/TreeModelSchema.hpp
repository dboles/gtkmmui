#ifndef DJB_GTKMMUI_TREEMODELSCHEMA_HPP
#define DJB_GTKMMUI_TREEMODELSCHEMA_HPP

#include "forward.hpp"

#include <gtkmm/treemodel.h>
#include <gtkmm/treemodelcolumn.h>

#include <concepts>
#include <functional>
#include <tuple>

namespace djb::gtkmmui {

template <typename... Types>
         requires (sizeof...(Types) > 0)
class TreeModelSchema final {
	Gtk::TreeModelColumnRecord m_record;
	std::tuple<Gtk::TreeModelColumn<Types>...> m_columns;

	[[nodiscard]] TreeModelSchema()
	{
		std::apply( [&](auto&... columns)
			{ (m_record.add(columns), ...); }, m_columns);
	}

public:
	[[nodiscard]] static auto const&
	singleton()
	{
		static auto const schema = TreeModelSchema{};
		return schema;
	}

	template <std::derived_from<Gtk::TreeModel> Store>
	[[nodiscard]] auto
	create() const
	{
		return Store::create(m_record);
	}

	decltype(auto)
	with_columns(auto&& function) const
	{
		return std::apply(FORWARD(function), m_columns);
	}
};

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_TREEMODELSCHEMA_HPP
