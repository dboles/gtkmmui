#ifndef DJB_GTKMMUI_CHILD_PROPERTY_HPP
#define DJB_GTKMMUI_CHILD_PROPERTY_HPP

#include "ChildPropertyGetter.hpp"

#include <gtkmm/container.h>
#include <gtkmm/widget.h>

#include <concepts>
#include <functional>
#include <utility>
#include <type_traits>

namespace djb::gtkmmui {

template <std::derived_from<Gtk::Container> Parent, typename Value,
          typename... Callbacks>
[[nodiscard]] auto
child_property(ChildPropertyGetter<Parent, Value> const getter,
               std::type_identity_t<Value> value,
               Callbacks... callbacks)
{
	return [ getter, value = std::move(value),
	         ...callbacks = std::move(callbacks) ]
	       (std::derived_from<Gtk::Widget> auto& child,
	        std::derived_from<Parent     > auto& parent)
	{
		auto proxy = (parent.*getter)(child);
		proxy.set_value(value);
		auto signal = proxy.signal_changed();
		(signal.connect(
			std::bind_front( callbacks, std::ref(parent), std::ref(child) )
		), ...);
	};
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_CHILD_PROPERTY_HPP
