#ifndef DJB_GTKMMUI_TOPLEVEL_HPP
#define DJB_GTKMMUI_TOPLEVEL_HPP

#include "IsToplevel.hpp"
#include "create.hpp"
#include "forward.hpp"

#include <concepts>
#include <functional>
#include <memory>
#include <utility>

namespace djb::gtkmmui {

template <IsToplevel Toplevel, typename... Args>
requires std::constructible_from<Toplevel, Args...>
[[nodiscard]] auto
toplevel(detail::CreateResult<Args...> create_args,
         std::invocable<Toplevel&> auto&&... functions)
{
	using namespace std;
	auto const make = [](auto... args)
	                  { return make_shared<Toplevel>(move(args)...); };
	auto toplevel = apply( make, move(create_args.tuple) );
	(std::invoke( FORWARD(functions), *toplevel.get() ), ...);
	return toplevel;
}

template <IsToplevel Toplevel>
[[nodiscard]] auto
toplevel(detail::IsNotCreateResult auto&&... functions)
{
	return toplevel<Toplevel>(detail::CreateResult<>{},
	                          FORWARD(functions)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_TOPLEVEL_HPP
