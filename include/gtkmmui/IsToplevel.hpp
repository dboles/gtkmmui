#ifndef DJB_GTKMMUI_ISTOPLEVEL_HPP
#define DJB_GTKMMUI_ISTOPLEVEL_HPP

#include <gtkmm/popover.h>
#include <gtkmm/window.h>
#include <concepts>

namespace djb::gtkmmui {

template <typename T>
concept IsToplevel = std::derived_from<T, Gtk::Window > or
                     std::derived_from<T, Gtk::Popover>;

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_ISTOPLEVEL_HPP
