#ifndef DJB_GTKMMUI_TITLEBAR_HPP
#define DJB_GTKMMUI_TITLEBAR_HPP

#include "widget.hpp"

#include <gtkmm/widget.h>
#include <gtkmm/window.h>

#include <concepts>
#include <utility>

namespace djb::gtkmmui {

template <std::derived_from<Gtk::Widget> Titlebar>
[[nodiscard]] auto
titlebar(std::invocable<Titlebar&> auto... functions)
{
	return [ ...functions = std::move(functions) ]
	       (Gtk::Window& window)
	{
		auto& titlebar = widget<Titlebar>(functions...);
		window.set_titlebar(titlebar);
	};
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_TITLEBAR_HPP
