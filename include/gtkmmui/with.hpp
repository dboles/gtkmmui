#ifndef DJB_GTKMMUI_WITH_HPP
#define DJB_GTKMMUI_WITH_HPP

#include "forward.hpp"

#include <glibmm/objectbase.h>

#include <concepts>
#include <functional>

namespace djb::gtkmmui::detail {

// TODO: better name?

template <std::derived_from<Glib::ObjectBase> Object>
void
with(Object& object,
     std::invocable<Object&> auto&&... functions)
{
	(std::invoke(FORWARD(functions), object), ...);
}

template <typename Function, typename Inner, typename Outer>
concept invocable_1or2 = std::invocable<Function, Inner&> or
                         std::invocable<Function, Inner&, Outer&>;

template <std::derived_from<Glib::ObjectBase> Inner,
          std::derived_from<Glib::ObjectBase> Outer,
          invocable_1or2<Inner, Outer> Function>
void
invoke(Function const& function, Inner& inner, Outer& outer)
{
	if constexpr( std::invocable<Function, Inner&, Outer&> ) {
		std::invoke(function, inner, outer);
	} else {
		std::invoke(function, inner);
	}
}

template <std::derived_from<Glib::ObjectBase> Inner,
          std::derived_from<Glib::ObjectBase> Outer>
requires ( not std::invocable<Outer, Inner&> )
void
with(Inner& inner, Outer& outer,
     invocable_1or2<Inner, Outer> auto&&... functions)
{
	(invoke(FORWARD(functions), inner, outer), ...);
}

} // namespace djb::gtkmmui::detail

#endif // DJB_GTKMMUI_WITH_HPP
