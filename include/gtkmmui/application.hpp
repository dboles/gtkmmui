#ifndef DJB_GTKMMUI_APPLICATION_HPP
#define DJB_GTKMMUI_APPLICATION_HPP

#include <giomm/application.h>
#include <functional>

namespace Glib { class ustring; }
namespace Gtk { class Application; }

namespace djb::gtkmmui {

using ActivateAppFunc = std::function< void (Gtk::Application&) >;
[[nodiscard]] auto run_application(Glib::ustring const& id,                        ActivateAppFunc) -> int;
[[nodiscard]] auto run_application(Glib::ustring const& id, Gio::ApplicationFlags, ActivateAppFunc) -> int;

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_APPLICATION_HPP
