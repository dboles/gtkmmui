#ifndef DJB_GTKMMUI_LIST_STORE_HPP
#define DJB_GTKMMUI_LIST_STORE_HPP

#include "TreeModel.hpp"
#include "forward.hpp"

#include <gtkmm/liststore.h>

#include <utility>

namespace djb::gtkmmui {

template <typename... Types>
[[nodiscard]] auto
list_store(detail::TreeModelInvocable<Gtk::ListStore, Types...> auto&&... functions)
{
	return detail::store<Gtk::ListStore, Types...>(FORWARD(functions)...);
}

template <typename... Types>
[[nodiscard]] auto
row(Types... values)
{
	return detail::tree_model_setter<Gtk::ListStore>(std::move(values)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_LIST_STORE_HPP
