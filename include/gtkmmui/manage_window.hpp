#ifndef DJB_GTKMMUI_MANAGE_WINDOW_HPP
#define DJB_GTKMMUI_MANAGE_WINDOW_HPP

#include <memory>

namespace Gtk {
	class Application;
	class Window;
}

namespace djb::gtkmmui {

void manage(std::shared_ptr<Gtk::Window>, Gtk::Application&);

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_MANAGE_WINDOW_HPP
