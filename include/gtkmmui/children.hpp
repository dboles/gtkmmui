#ifndef DJB_GTKMMUI_CHILDREN_HPP
#define DJB_GTKMMUI_CHILDREN_HPP

#include "ensure.hpp"

#include <gtkmm/container.h>

#include <concepts>
#include <functional>
#include <utility>

namespace djb::gtkmmui {

[[nodiscard]] auto
children(int const count, std::invocable<int> auto function)
{
	DJB_ENSURE(count >= 0);

	return [ count, function = std::move(function) ]
	       (std::derived_from<Gtk::Container> auto& parent)
	{
		for (auto i = 0; i < count; ++i) {
			std::invoke(std::invoke(function, i), parent);
		}
	};
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_CHILDREN_HPP
