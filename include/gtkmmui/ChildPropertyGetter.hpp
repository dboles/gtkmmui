#ifndef DJB_GTKMMUI_CHILDPROPERTYGETTER_HPP
#define DJB_GTKMMUI_CHILDPROPERTYGETTER_HPP

#include <gtkmm/childpropertyproxy.h>
#include <gtkmm/container.h>

#include <concepts>

namespace djb::gtkmmui {

template <std::derived_from<Gtk::Container> Parent, typename Value>
using ChildPropertyGetterConst = Gtk::ChildPropertyProxy_ReadOnly<Value>
                                 (Parent::*)(Gtk::Widget const&) const;

template <std::derived_from<Gtk::Container> Parent, typename Value>
using ChildPropertyGetter = Gtk::ChildPropertyProxy<Value>
                            (Parent::*)(Gtk::Widget&);

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_CHILDPROPERTYGETTER_HPP
