#ifndef DJB_GTKMMUI_CALLNOW_HPP
#define DJB_GTKMMUI_CALLNOW_HPP

namespace djb::gtkmmui {

enum class CallNow {no, yes};

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_CALLNOW_HPP
