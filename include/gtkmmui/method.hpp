#ifndef DJB_GTKMMUI_METHOD_HPP
#define DJB_GTKMMUI_METHOD_HPP

#include <glibmm/objectbase.h>
#include <concepts>
#include <functional>

namespace djb::gtkmmui {

template <std::derived_from<Glib::ObjectBase> Object,
          typename Result, typename... Args>
[[nodiscard]] auto
method(Result (Object::* const method)(Args...), auto&&... args)
{
	return std::bind(method, std::placeholders::_1, std::move(args)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_METHOD_HPP
