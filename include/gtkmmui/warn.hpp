#ifndef DJB_GTKMMUI_WARN_HPP
#define DJB_GTKMMUI_WARN_HPP

#include "gtkmmui/format.hpp"
#include "gtkmmui/forward.hpp"

#include <glib.h>

namespace djb::gtkmmui {

template <typename... Args>
constexpr void
warn(fmt::format_string<Args...> const fmt_str, Args&&... args)
{
	g_warning( "gtkmmui: warning: %s\n",
	           fmt::format(fmt_str, FORWARD(args)...).c_str() );
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_WARN_HPP
