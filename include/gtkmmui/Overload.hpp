#ifndef DJB_GTKMMUI_OVERLOAD_HPP
#define DJB_GTKMMUI_OVERLOAD_HPP

#include <concepts>
#include <type_traits>

namespace djb::gtkmmui {

template <typename... Ts>
         requires (sizeof...(Ts) >= 2)
struct Overload final: Ts... {
	using Ts::operator()...;
};

template <typename... Ts>
Overload(Ts&&...) -> Overload<std::remove_reference_t<Ts>...>;

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_OVERLOAD_HPP
