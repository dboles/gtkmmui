#ifndef DJB_GTKMMUI_CREATE_HPP
#define DJB_GTKMMUI_CREATE_HPP

#include <tuple>
#include <utility>

namespace djb::gtkmmui {

namespace detail {

template <typename... Ts>
struct CreateResult final { std::tuple<Ts...> tuple; };

template <typename T>
concept IsNotCreateResult = not std::same_as< T, CreateResult<> >;

} // namespace detail

template <typename... Ts>
[[nodiscard]] auto
create(Ts... args)
{
	return detail::CreateResult<Ts...>{ {std::move(args)...} };
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_CREATE_HPP
