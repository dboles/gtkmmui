#ifndef DJB_GTKMMUI_CHILD_NOTIFY_HPP
#define DJB_GTKMMUI_CHILD_NOTIFY_HPP

#include "ChildPropertyGetter.hpp"

#include <gtkmm/container.h>
#include <gtkmm/widget.h>
#include <sigc++/functors/mem_fun.h>

#include <concepts>
#include <functional>
#include <utility>

namespace djb::gtkmmui {

template <std::derived_from<Gtk::Container> Parent, typename Value,
          typename... Callbacks>
requires (sizeof...(Callbacks) > 0)
[[nodiscard]] auto
child_notify(ChildPropertyGetterConst<Parent, Value> const getter,
             Callbacks... callbacks)
{
	return [ getter, ...callbacks = std::move(callbacks) ]
               (std::derived_from<Gtk::Widget> auto& child,
                std::derived_from<Parent     > auto& parent)
	{
		auto proxy = (parent.*getter)(child);
		auto signal = proxy.signal_changed();
		(signal.connect(
			std::bind_front( callbacks, std::ref(parent), std::ref(child ) )
		), ...);
	};
}

template <std::derived_from<Gtk::Container> Parent, typename Value,
          typename MemObj, typename MemRes, typename... MemArgs>
[[nodiscard]] auto
child_notify(ChildPropertyGetterConst<Parent, Value> const getter,
             MemObj& mem_obj, MemRes (MemObj::* const mem_ptr)(MemArgs...) )
{
	return child_notify( getter, sigc::mem_fun(mem_obj, mem_ptr) );
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_CHILD_NOTIFY_HPP
