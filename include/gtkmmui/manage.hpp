#ifndef DJB_GTKMMUI_MANAGE_HPP
#define DJB_GTKMMUI_MANAGE_HPP

namespace Glib {
	class ObjectBase;
	template <typename> class RefPtr;
}

namespace djb::gtkmmui::detail {

/* Glib::Binding overrides GBindingʼs reference-counting. GBinding will be
   freed when either of the bound objects is or when unbind() is called on it.
   However, Glib::Binding is only unreferenced when the final RefPtr to it goes
   out of scope. So, keep a RefPtr to each binding for each bound object. The
   end result of all this is basically getting the C-style lifetime management
   back… so that bindings are automatically managed by their objects in gtkmmui.
   The same goes for Gtk::Gesture, which normally needs a RefPtr kept around.
   Preferable would be an opt-in to C-style lifetime, to avoid all this! Anyway,
   see https://gitlab.gnome.org/GNOME/glibmm/issues/62 for discussion on this */

void manage(Glib::RefPtr<Glib::ObjectBase const> managed,
            Glib::ObjectBase const& manager);

} // namespace djb::gtkmmui::detail

#endif // DJB_GTKMMUI_MANAGE_HPP
