#ifndef DJB_GTKMMUI_TREEMODEL_HPP
#define DJB_GTKMMUI_TREEMODEL_HPP

#include "TreeModelSchema.hpp"
#include "format.hpp"
#include "forward.hpp"

#include <gtkmm/treeiter.h>
#include <gtkmm/treemodel.h>

#include <concepts>
#include <functional>
#include <optional>
#include <tuple>
#include <type_traits>
#include <utility>

namespace djb::gtkmmui {

template <std::derived_from<Gtk::TreeModel> Store, typename... Types>
struct TreeModelData final {
	using Schema = TreeModelSchema<Types...>;
	Schema const& schema;
	Glib::RefPtr<Store> const store;

	[[nodiscard]] TreeModelData()
	: schema{ Schema::singleton() },
	  store{ schema.template create<Store>() }
	{}
};

template <typename... Types>
[[nodiscard]] auto
get_values(TreeModelSchema<Types...> const& schema, Gtk::TreeRow const& row)
{
	return schema.with_columns( [&](auto&... columns)
		{ return std::tuple{row.get_value(columns)...}; } );
}

template <typename... Types>
void
set_values(TreeModelSchema<Types...> const& schema, Gtk::TreeRow const& row,
           std::type_identity_t<Types> const&... values)
{
	schema.with_columns( [&](auto&... columns)
		{ (row.set_value(columns, values), ...); } );
}

template <typename... Types>
[[nodiscard]] auto
get_proxies(TreeModelSchema<Types...> const& schema, Gtk::TreeRow const& row)
{
	return schema.with_columns( [&](auto&... columns)
		{ return std::tuple{row[columns]...}; } );
}

template <typename Store, typename... Types,
          typename Function> requires std::is_invocable_r_v<bool, Function,
                                                            Gtk::TreeValueProxy<Types>...>
void
foreach(TreeModelData<Store, Types...> const& data, Function&& function)
{
	data.store->foreach_iter( [&](Gtk::TreeIter const& it)
	{
		return std::apply( FORWARD(function),
		                   get_proxies(data.schema, *it) );
	} );
}

namespace detail {

template <typename Function, typename Store, typename ...Types>
concept TreeModelInvocable = std::invocable<Function,
                                            TreeModelData<Store, Types...> const&>;

template <std::derived_from<Gtk::TreeModel> Store, typename... Types>
[[nodiscard]] auto
store(TreeModelInvocable<Store, Types...> auto&&... functions)
{
	auto data = TreeModelData<Store, Types...>{};
	(std::invoke(FORWARD(functions), data), ...);
	return data;
}

template <std::derived_from<Gtk::TreeModel> Store, typename... Types>
[[nodiscard]] auto
tree_model_setter(Types... values)
{
	return [ ...values = std::move(values) ]
	       <typename... DataTypes>
	       ( TreeModelData<Store, DataTypes...> const& data,
	         std::optional<Gtk::TreeRow> row = {} )
	{
		if (not row) row = *data.store->append();
		set_values(data.schema, *row, values...);
	};
}

} // namespace detail

} // namespace djb::gtkmmui

template <>
template <typename T>
struct fmt::formatter< Gtk::TreeValueProxy<T> > final: formatter<T>
{
	[[nodiscard]] auto format(Gtk::TreeValueProxy<T> const& proxy, auto& context) const
	{ return formatter<T>::format(proxy.operator T(), context); }
};

#endif // DJB_GTKMMUI_TREEMODEL_HPP
