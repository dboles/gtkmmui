#ifndef DJB_GTKMMUI_CONTROLLER_HPP
#define DJB_GTKMMUI_CONTROLLER_HPP

#include "create.hpp"
#include "forward.hpp"
#include "manage.hpp"

#include <glibmm/refptr.h>
#include <gtkmm/eventcontroller.h>
#include <gtkmm/widget.h>

#include <concepts>
#include <functional>
#include <utility>

namespace djb::gtkmmui {

namespace detail {

template <std::derived_from<Gtk::EventController> Controller,
          std::derived_from<Gtk::Widget         > Widget    >
void
invoke_manage(Glib::RefPtr<Controller> controller, Widget& widget,
              std::invocable<Controller&> auto&&... functions)
{
	(std::invoke( FORWARD(functions), *controller.get() ), ...);
	manage(std::move(controller), widget);
}

} // namespace detail

template <std::derived_from<Gtk::EventController> Controller,
          typename... Args, typename... Functions>
requires std::invocable<decltype(&Controller::create), Gtk::Widget&, Args...>
[[nodiscard]] auto
controller(detail::CreateResult<Args...> create_args, Functions... functions)
{
	return [ create_args = std::move(create_args),
	         ...functions = std::move(functions) ]
	       <std::derived_from<Gtk::Widget> Widget>
	       requires (std::invocable<Functions, Controller&> and ...)
	       (Widget& widget)
	{
		auto refptr = std::apply(std::bind_front( &Controller::create,
		                                          std::ref(widget) ),
		                         create_args.tuple);
		detail::invoke_manage(std::move(refptr), widget, functions...);
	};
}

template <std::derived_from<Gtk::EventController> Controller>
[[nodiscard]] auto
controller(detail::IsNotCreateResult auto... functions)
{
	return controller<Controller>(detail::CreateResult<>{}, std::move(functions)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_CONTROLLER_HPP
