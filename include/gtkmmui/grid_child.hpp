#ifndef DJB_GTKMMUI_GRID_CHILD_HPP
#define DJB_GTKMMUI_GRID_CHILD_HPP

#include "child.hpp"

#include <gtkmm/grid.h>

#include <concepts>
#include <utility>

namespace djb::gtkmmui {

template <std::derived_from<Gtk::Widget> Widget,
          typename... Args>
requires std::constructible_from<Widget, Args...>
[[nodiscard]] auto
grid_child(int const left_attach, int const top_attach,
           int const width, int const height,
           detail::CreateResult<Args...> create_args,
           std::invocable<Widget&> auto... functions)
{
	auto const attach = [=](Widget& widget, Gtk::Grid& grid)
	{
		if (left_attach != 0) {
			grid.child_property_left_attach(widget) = left_attach;
		}

		if (top_attach != 0) {
			grid.child_property_top_attach(widget) = top_attach;
		}

		if (width != 1) {
			grid.child_property_width(widget) = width;
		}

		if (height != 1) {
			grid.child_property_height(widget) = height;
		}
	};

	return child<Widget>(std::move(create_args),
	                     attach, std::move(functions)...);
}

template <std::derived_from<Gtk::Widget> Widget>
[[nodiscard]] auto
grid_child(int const left_attach, int const top_attach,
           int const width, int const height,
           detail::IsNotCreateResult auto... functions)
{
	return grid_child<Widget>(left_attach, top_attach,
	                          width, height,
	                          detail::CreateResult<>{},
				  std::move(functions)...);
}

template <std::derived_from<Gtk::Widget> Widget,
          typename... Args>
requires std::constructible_from<Widget, Args...>
[[nodiscard]] auto
grid_child(int const left_attach, int const top_attach,
           detail::CreateResult<Args...> create_args,
           std::invocable<Widget&> auto... functions)
{
	return grid_child<Widget>(left_attach, top_attach, 1, 1,
	                          std::move(create_args),
	                          std::move(functions)...);
}

template <std::derived_from<Gtk::Widget> Widget>
[[nodiscard]] auto
grid_child(int const left_attach, int const top_attach,
           detail::IsNotCreateResult auto... functions)
{
	return grid_child<Widget>(left_attach, top_attach,
	                          detail::CreateResult<>{},
	                          std::move(functions)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_GRID_CHILD_HPP
