#ifndef DJB_GTKMMUI_C2USTRING_HPP
#define DJB_GTKMMUI_C2USTRING_HPP

#include <glibmm/ustring.h>
#include <concepts>
#include <type_traits>

namespace djb::gtkmmui {

template <typename T>
using C2Ustring = std::conditional_t<std::convertible_to<T, Glib::ustring>,
                                     Glib::ustring, T>;

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_C2USTRING_HPP
