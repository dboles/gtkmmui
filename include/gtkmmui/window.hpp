#ifndef DJB_GTKMMUI_WINDOW_HPP
#define DJB_GTKMMUI_WINDOW_HPP

#include "create.hpp"
#include "forward.hpp"
#include "manage_window.hpp"
#include "toplevel.hpp"

#include <gtkmm/window.h>

#include <concepts>
#include <utility>

namespace djb::gtkmmui {

template <std::derived_from<Gtk::Window> Window, typename... Args>
requires std::constructible_from<Window, Args...>
[[nodiscard]] auto
window(detail::CreateResult<Args...> create_args,
       Gtk::Application& application,
       std::invocable<Window&> auto&&... functions)
{
	auto window = toplevel<Window>(std::move(create_args),
	                               FORWARD(functions)...);
	manage(window, application);
	return window;
}

template <std::derived_from<Gtk::Window> Window>
[[nodiscard]] auto
window(Gtk::Application& application,
       std::invocable<Window&> auto&&... functions)
{
	return window<Window>(detail::CreateResult<>{},
	                      application,
	                      FORWARD(functions)...);
}

} // namespace djb::gtkmmui

#endif // DJB_GTKMMUI_WINDOW_HPP
